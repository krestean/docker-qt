### Create base image first
```docker build --force-rm -f Dockerfile.mxe -t jk/mxe .```


### Download and extract python and pybind11 from msys2
```
https://packages.msys2.org/package/mingw-w64-x86_64-python
https://packages.msys2.org/package/mingw-w64-x86_64-pybind11
tar -I zstd -xvf ./mingw-w64-x86_64*
```
this should create a mingw64 folder

### Create images based on the base image
```
docker build --force-rm -f Dockerfile.win64d -t jk/qt:win64d .
docker build --force-rm -f Dockerfile.win64s -t jk/qt:win64s .
```

### Run image from project directory
```
docker run --rm -it -v $(pwd):/app jk/qt:win64s


cd /app/build
cmake .. 
make -j12
```

### If making dynamic build, copy  qt dlls
```
mxedeployqt --mxepath /opt/mxe/usr --mxetarget x86_64-w64-mingw32.shared --qmlrootpath=/app/src /app/build
```

Additional dlls needed for gnuradio, not shure if I caught 'em all.
```
/opt/log4cpp/build/liblog4cpp.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/lib/libquazip5.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libboost_filesystem-mt-x64.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libboost_program_options-mt-x64.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libboost_thread_pthread-mt-x64.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libgmp-10.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libvolk.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libgnuradio-pmt.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libgnuradio-runtime.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libgnuradio-fft.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libgnuradio-analog.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libgnuradio-filter.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/bin/libfftw3f-3.dll  
/opt/mxe/usr/x86_64-w64-mingw32.shared/qt5/lib/qwt.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/qt5/bin/Qt5OpenGL.dll
/opt/mxe/usr/x86_64-w64-mingw32.shared/qt5/bin/Qt5Charts.dll
```

### Windoes Server Deployment
Opengl32 dll is needed for QtQuck / QML to work, as Win Server comes with no graphics drivers, or too old opengl version.
Sofware rendrer dll "opengl32sw.dll" can be obtained from:
```
http://download.qt.io/development_releases/prebuilt/llvmpipe/windows/
```
The latest version, opengl32sw-64-mesa_11_2_2-signed.7z worked with windows server 2012 r2.
